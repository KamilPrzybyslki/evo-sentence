#!/usr/bin/ruby

class Evo_sentence
  attr_accessor :sentence
  attr_reader :Children_number

  #getting user's sentence from command line argument
  #and changing it tu upcase
  def initialize
    @sentence = ARGV[0].upcase

    #number of children in each generation
    @Children_number = 50
  end

  #Generates single gene (letter).
  def random_gene
    gene = (65 + Random.rand(27)).chr  #65 is "A", 90 is "Z", 91 is "["
    if gene == "[" then gene = " "
    end
    gene
  end
  # Generates first random sentences which will evolve
  # space is separated in ASCII code from letters so whenever
  # random reaches above "Z" it convers it into space char.
  def begin_random
    phrase = String.new

    for i in 0...self.sentence.length do
      phrase[i] = self.random_gene
    end
    phrase
  end

  # Breeds parent to children wich small mutations.
  # Returns array of children
  def breed parent
    children = Array.new
    self.Children_number.times do
      position = Random.rand(parent.length)
      child = String.new(parent)
      child[position] = random_gene
      # puts "#{child} rate: #{self.rate_child(child, self.sentence)}"

    children.push(child)
    end
    children
  end

  # Compares signs between child and given sentence. The more signs match,
  # the more rate grows.
  def rate_child child, sentence
    rate = 0
    for i in 0..child.length do
      if child[i] == sentence[i] then
        rate += 1
        # print "#{child[i]}=>#{i} "
      end
    end
    rate
  end

  def choose_best_child children
    best_rate = 0
    best_child = ""
    children.each {|child|
      r = rate_child(child, self.sentence)
      if r >= best_rate then
        best_child = child
        best_rate = r
      end
    }
    best_child
  end

end

evo = Evo_sentence.new
puts evo.sentence
rodzic = evo.begin_random
puts rodzic, "\n"
puts "szukamy wybrańca"
iteracja = 1
while !rodzic.eql?(evo.sentence)
  rodzic = evo.choose_best_child(evo.breed rodzic)
  puts "#{rodzic} iteracja #{iteracja}"
  iteracja += 1
end
# puts "Wybraniec: #{evo.choose_best_child(evo.breed rodzic)}"
